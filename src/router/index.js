import Vue from 'vue'
import Router from 'vue-router'
import Menu from '@/pages/Menu'
import CreateSession from '@/pages/CreateSession'
import NotFound from '@/pages/NotFound'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '',
      name: 'Menu',
      component: Menu,
    },
    {
      path: '/create-session/',
      name: 'CreateSession',
      component: CreateSession,
    },
    {
      path: '(.*)',
      name: 'NotFound',
      component: NotFound,
    },
  ]
})
